//
//  SchoolDetailViewModel.swift
//  NYCSchoolsWithCombine
//
//  Created by Zulfiqur Ali on 12/29/22.
//

import Foundation
import Combine


class SchoolDetailViewModel {
    
    private let network: Network
    private let school: School
    private var subs = Set<AnyCancellable>()
    
    @Published var schoolSATScores: SchoolSATScores?

    init(network: Network, school: School) {
        self.network = network
        self.school = school
    }
    
    func requestSATScores() {
        self.network.requestModel(request: Environment.schoolDetails(self.school.dbn).request)
            .receive(on: DispatchQueue.main)
            .sink { completion in
                print(completion)
            } receiveValue: { (schoolSAT: [SchoolSATScores]) in
                self.schoolSATScores = schoolSAT.first
            }.store(in: &self.subs)
    }
    
    var schoolName: String {
        return self.school.schoolName
    }
    
    var schoolOverview: String {
        return self.school.overviewParagraph
    }
    
    var zipcode: String {
        return self.school.zip
    }
    
    var address: String {
        return "\(self.school.primaryAddressLine1), \(self.school.city), \(self.school.stateCode), \(self.school.zip)"
    }
    
    var readingScore: String {
        return "Reading: \(self.schoolSATScores?.readingScore ?? "N/A")"
    }
    
    var mathScore: String {
        return "Math: \(self.schoolSATScores?.mathScore ?? "N/A")"
    }
    
    var writingScore: String {
        return "Writing: \(self.schoolSATScores?.writingScore ?? "N/A")"
    }
    
    var latitude: String? {
        return self.school.latitude
    }
    
    var longitude: String? {
        return self.school.longitude
    }
    
}
