//
//  NetworkError.swift
//  NYCSchoolsWithCombine
//
//  Created by Zulfiqur Ali on 12/29/22.
//

import Foundation

enum NetworkError: Error {
    case badRequest
    case badStatusCode(Int)
    case decodeError(Error)
    case other(Error)
}
