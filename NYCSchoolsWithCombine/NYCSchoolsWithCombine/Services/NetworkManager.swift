//
//  NetworkManager.swift
//  NYCSchoolsWithCombine
//
//  Created by Zulfiqur Ali on 12/29/22.
//

import Foundation
import Combine

protocol Network {
    func requestModel<T: Decodable>(request: URLRequest?) -> AnyPublisher<T, NetworkError>
}

class NetworkManager {
    
    let session: URLSession
    let decoder: JSONDecoder
    
    init(session: URLSession = URLSession.shared, decoder: JSONDecoder = JSONDecoder(), decodeStrategy: JSONDecoder.KeyDecodingStrategy? = .convertFromSnakeCase) {
        self.session = session
        if let strategy = decodeStrategy {
            decoder.keyDecodingStrategy = strategy
        }
        self.decoder = decoder
    }
    
}

extension NetworkManager: Network {
    
    func requestModel<T: Decodable>(request: URLRequest?) -> AnyPublisher<T, NetworkError> {
        
        guard let request = request else {
            return Fail(error: NetworkError.badRequest).eraseToAnyPublisher()
        }
        
        return self.session.dataTaskPublisher(for: request)
            .tryMap { payload in
                if let httpResponse = payload.response as? HTTPURLResponse,
                   !(200..<300).contains(httpResponse.statusCode) {
                    throw NetworkError.badStatusCode(httpResponse.statusCode)
                }
                return payload.data
            }
            .decode(type: T.self, decoder: self.decoder)
            .mapError { error in
                if let decodeErr = error as? DecodingError {
                    return NetworkError.decodeError(decodeErr)
                }
                return NetworkError.other(error)
            }.eraseToAnyPublisher()
        
    }
    
}


